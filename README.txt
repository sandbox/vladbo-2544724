Requirements:
- Enable necessary modules (Domain, Domain Conf, Domain Folder,
  if language is needed - Domain Locale).
- Disable checkbox "Force domain editing from the primary domain"
  in "Structure" > "Domains" > "Settings".
- Go to "Structure" > "Domains" and add there needed domains.

Optional:
- Enable some languages if they are needed in Localization settings.
- Enable needed for domain language in Domain Languagues settings.
