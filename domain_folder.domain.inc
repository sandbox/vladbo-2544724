<?php

/**
 * @file
 * Domain hooks for Domain Folder.
 *
 * @ingroup domain_folder
 */

/**
 * Implements hook_domain_path().
 *
 * Allows modules to alter path when rewriting URLs.
 *
 * This hook will fire for all paths and may be resource-intensive.
 * Look at Domain Prefix for best practices implementation. In Domain
 * Prefix, we only include this function if we know it is necessary.
 *
 * @see domain_prefix_init()
 * @see hook_url_outbound_alter()
 */
function domain_folder_domain_path($domain_id, &$path, &$options, $original_path) {
  $default_id = domain_default_id();
  if ($domain_id == $default_id) {
    $current_domain = domain_get_domain();
    if ($current_domain != -1 && $current_domain['domain_id'] != $default_id) {
      $domain_suffix = domain_conf_variable_get($current_domain['domain_id'], 'domain_suffix', FALSE, TRUE);
      if ($domain_suffix) {
        $prefix = arg(0, $path);
        if ($prefix == $domain_suffix['suffix']) {
          $path = substr($path, strlen($domain_suffix['suffix']) + 1);
        }
      }
    }
  }
}

/**
 * Implements hook_domain_load().
 *
 * Adds a suffix for the current domain.
 */
function domain_folder_domain_load(&$domain) {
  // Check if domain bootstrap is loaded.
  if (function_exists('_domain_bootstrap_modules')) {
    $domain['suffix'] = domain_conf_variable_get($domain['domain_id'], 'domain_suffix');
  }
}

/**
 * Implements hook_domain_load().
 *
 * The form values processed by the form. Note that these are not editable
 * since module_invoke_all() cannot pass by reference. They are passed in
 * case some module needs to check the original form input.
 */
function domain_folder_domain_delete($domain, $form_values = array()) {
  $secure_hosts = variable_get('secure_hosts', array());
  unset($secure_hosts[$domain['domain_id']]);
  $domain_id = $domain['domain_id'];
  variable_set('secure_hosts', $secure_hosts);

  $active_domains  = variable_get('domain_active_domains', array());
  unset($active_domains[$domain_id]);
  variable_set('domain_active_domains', $active_domains);
}
